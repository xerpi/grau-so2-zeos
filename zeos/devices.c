#include <io.h>
#include <utils.h>
#include <list.h>
#include <devices.h>

// Queue for blocked processes in I/O
struct list_head blocked;

static Byte tmpbuf[READ_BUFF_SIZE];
cbuffer_t cb;
int read_pending;

void init_devices()
{
	cbuffer_init(&cb);
	read_pending = 0;
}

int sys_write_console(char *buffer, int size)
{
	int i;

	for (i = 0; i < size; i++)
		printc(buffer[i]);

	return size;
}


int sys_read_keyboard()
{
	int n_min, ret;
	struct task_struct *cur = current();

	if (!list_empty(&keyboardqueue) || read_pending) {
		update_process_state_rr(cur, &keyboardqueue);
		sched_next_rr();
	}

	while (cur->kbs.remain_read_char > 0) {
		n_min = min(cur->kbs.remain_read_char, READ_BUFF_SIZE);

		ret = cbuffer_read(&cb, (char *)tmpbuf, n_min);
		copy_to_user(tmpbuf, cur->kbs.usr_buffer, ret);

		cur->kbs.usr_buffer += ret;
		cur->kbs.remain_read_char -= ret;

		if (cur->kbs.remain_read_char > 0) {
			cur->state = ST_BLOCKED;
			// HACK: cur pointer is stored in register,
			// so when we return to this context cur = undefined
			list_add(&(current()->list), &keyboardqueue);
			sched_next_rr();
		}
	}

	read_pending = 0;

	return cur->kbs.total_read_char;
}
