#include <libc.h>

char buff[24];

int pid;
int a;

void printi(int i)
{
	char buffer[16];
	itoa(i, buffer);
	printf(buffer);
}

#define print_attribute(s, a) \
	do { \
		printf("  " #a ":   "); \
		printi(s.a); \
		printf("\n"); \
	} while (0)

#define printsi(s, i) \
	do { \
		printf(s); \
		printi((int)i); \
		printf("\n"); \
	} while (0)

volatile int sum = 0;
volatile int thread_done1 = 0;
volatile int thread_done2 = 0;

static void inc_sum()
{
	volatile int j = sum;

	volatile int i;
	for (i = 0; i <= 1000000; i++) {
	}
	j++;
	sum = j;
}

static void thread_fn1(void)
{
	printf("thread func\n");

	int ret = sem_wait(0);
	printsi("thread 1 wait: ", errno);

	printf("enter\n");

	inc_sum();

	sem_signal(0);

	printf("done\n");

	thread_done1 = 1;

	//printi(sum);
	exit();
}

static void thread_fn2(void)
{
	printf("thread func\n");

	int ret = sem_wait(0);
	printsi("thread 2 wait: ", errno);

	printf("enter\n");

	inc_sum();

	sem_signal(0);

	printf("done\n");

	thread_done2 = 1;

	//printi(sum);
	exit();
}


int __attribute__ ((__section__(".text.main")))
  main(void)
{
/*	char thread_stack1[1024];
	char thread_stack2[1024];
	//int i, x, pid;
	//char s[5*1024];
	//for (i = 0; i < 5*1024; ++i) s[i] = 'a';

	sem_init(0, 1);

	clone(thread_fn1, thread_stack1 + 1024);
	clone(thread_fn2, thread_stack2 + 1024);

	sem_wait(0);
	printf("main enter\n");

	inc_sum();

	//sem_signal(0);
	sem_destroy(0);
	sem_init(0, 1);

	printf("main done\n");

	while (!(thread_done1 && thread_done2)) ;

	printi(sum);


	while (1);

*/
/*
	char buf[12] = "-----------";
	int ret;

	ret = fork();
	if (ret == 0) {
		int pid2 = fork();
		if (pid2 == 0) {

		} else {

		}

		while (1) {
			printf("HIJO\n");
			ret = read(0, buf, 4);
			//printsi("ret: ", ret);
			//printf("\n");
			printf(buf);
			printf("\n");
		}
	}
	while (1) {
		printf("PADRE\n");
		ret = read(0, buf, 6);
		//printsi("ret: ", ret);
		//printf("\n");
		printf(buf);
		printf("\n");
	}
*/
/*
	void *ret = sbrk(0);

	printsi("sbrk(0) = ", ret);
	ret = sbrk(4096);
	*(char *)(ret + 4095) = 'A';
	printsi("ret = ", *(char *)(ret + 4095));


	void *ret1 = sbrk(-4094);
	*(char *)(ret + 4095) = 'B';
	printsi("ret = ", *(char *)(ret + 4095));



	printsi("sbrk(0) = ", ret);
	ret = sbrk(0);
	printsi("sbrk(0) = ", ret);

	while (1) {


	}
*/
	void *ret = sbrk(0);

	printsi("sbrk(0) = ", ret);
	ret = sbrk(0x20000);
	*(char *)(ret + 0x20000 - 1) = 'A';

	int ret1 = fork();
	if (ret1 == 0) {
		printsi("HIJO: ", *(char *)(ret + 0x20000 - 1));
	} else {
		printsi("PADRE: ", *(char *)(ret + 0x20000 - 1));
	}
	while (1) {


	}


	/*x = fork();
	pid = getpid();

	if (x == 0) {
		printf("child\n");
		//exit();
		printf("child after exit()\n");
	} else if (x > 0) {
		printf("parent\n");
	}

	//x = getpid();

	// En este caso se ve que al pasar p se esrcibe EFAULT (14)
	//x = write(1, s, strlen(s));
	//printf("\n");
	itoa(x, s);*/

	//itoa(errno, s);
	//write(1, s, strlen(s));
	//while (1) {
		/*struct stats st;
		get_stats(pid, &st);

		printf("PID: "); printi(pid); printf("\n");

		print_attribute(st, user_ticks);
		print_attribute(st, system_ticks);
		print_attribute(st, blocked_ticks);
		print_attribute(st, ready_ticks);
		print_attribute(st, elapsed_total_ticks);
		print_attribute(st, total_trans);
		print_attribute(st, remaining_ticks);*/

		//printf("User process\n");
		//x = write(1, s, strlen(s));
		//printf("\n");
	//}

	/*char c[100];

	while (1) {
		itoa(a, c);
		printf("THREAD 0: ");
		printf(c);
		printf("\n");
		++a;
	}*/

	//exit();
	return 0;
}
