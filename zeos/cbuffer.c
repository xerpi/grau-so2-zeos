#include <cbuffer.h>
#include <utils.h>

void cbuffer_init(cbuffer_t *cb)
{
	cb->remain_slot = C_SIZE;
	cb->start = 0;
	cb->end = 0;
}

int cbuffer_read(cbuffer_t *cb, char *buffer, unsigned int size)
{
	unsigned int n_min;
	char * tmp_buffer = buffer;
	n_min = min(size, C_SIZE - cb->remain_slot);

	if (cb->start < cb->end) {
		copy_data(&cb->carray[cb->start], tmp_buffer, n_min);
	} else {
		int calc = C_SIZE - cb->start;
		if (cb->start + n_min >= C_SIZE) {

			copy_data(&cb->carray[cb->start], tmp_buffer, calc);
			tmp_buffer += calc;
			copy_data(&cb->carray[0], tmp_buffer, n_min - calc);

		}
		else {
			copy_data(&cb->carray[cb->start], tmp_buffer, n_min);
		}
	}

	cb->start = (cb->start + n_min)%C_SIZE;
	cb->remain_slot += n_min;

	return n_min;
}

int cbuffer_write(cbuffer_t *cb, char *buffer, unsigned int size)
{
	unsigned int n_min;
	char *tmp_buffer = buffer;
	n_min = min(size, cb->remain_slot);

	if (cb->start > cb->end) {
		copy_data(tmp_buffer, &cb->carray[cb->end], n_min);
	} else {
		int calc = C_SIZE - cb->end;
		if (cb->end + n_min >= C_SIZE) {
			copy_data(tmp_buffer, &cb->carray[cb->end], calc);
			tmp_buffer += calc;
			copy_data(tmp_buffer, &cb->carray[0], n_min - calc);

		}
		else {
			copy_data(tmp_buffer, &cb->carray[cb->end], n_min);
		}

	}
	cb->end = (cb->end + n_min)%C_SIZE;
	cb->remain_slot -= n_min;

	return n_min;
}
