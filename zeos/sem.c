#include "sem.h"

struct seminfo_t seminfo[SEMINFO_SIZE];


void init_sem()
{
	int i;
	for (i = 0; i < SEMINFO_SIZE; i++) {
		seminfo[i].owner = -1;
		seminfo[i].numwait = 0;
	}
}
