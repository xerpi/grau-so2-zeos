/*
 * io.c -
 */

#include <io.h>

#include <types.h>

/**************/
/** Screen  ***/
/**************/

#define NUM_COLUMNS 80
#define NUM_ROWS    25

static Byte cursor_x = 0, cursor_y = 0;
static Word console_color = (15 << 8);

/* 15      12 11      8 7        0
 * +---------+---------+---------+
 * |Backcolor|Forecolor|Character|
 * +---------+---------+---------+
 */

/* Read a byte from 'port' */
Byte inb(unsigned short port)
{
	Byte v;
	__asm__ __volatile__ ("inb %w1,%0":"=a" (v):"Nd" (port));
	return v;
}

/* Write 'b' to 'port' */
void outb(unsigned short port, Byte b)
{
	__asm__ __volatile__ ( "movb %0, %%al; outb %1" :: "a"(b), "d"(port));
}

void console_set_color(Word color)
{
	console_color = color & 0x0F00;
}

static void console_scroll_down()
{
	int i, j;
	for (i = 0; i < NUM_ROWS - 1; i++) {
		for (j = 0; j < NUM_COLUMNS - 1; j++) {
			*(Word *)(VGA_TEXT_SCREEN_MEM + (i * NUM_COLUMNS + j) * 2) =
				*(Word *)(VGA_TEXT_SCREEN_MEM + ((i + 1) * NUM_COLUMNS + j) * 2);
		}
	}

	/* Clear the last line */
	for (j = 0; j < NUM_COLUMNS; j++) {
		*(Word *)(VGA_TEXT_SCREEN_MEM + ((NUM_ROWS - 1) * NUM_COLUMNS + j) * 2) = 0;
	}
}

void printc(char c)
{
	if (!c)
		return;

	outb(0xe9, (Byte)c);

	if (c == '\n') {
		cursor_x = 0;
		if (cursor_y == NUM_ROWS - 1)
			console_scroll_down();
		else
			cursor_y++;
	} else {
		*(Word *)(VGA_TEXT_SCREEN_MEM + (cursor_y * NUM_COLUMNS + cursor_x) * 2) =
			(Word) (c & 0x007F) | console_color;

		if (++cursor_x >= NUM_COLUMNS) {
			cursor_x = 0;
			if (cursor_y == NUM_ROWS - 1)
				console_scroll_down();
			else
				cursor_y++;
		}
	}
}

void printc_xy(Byte mx, Byte my, char c)
{
	Byte cx, cy;
	cx = cursor_x;
	cy = cursor_y;
	cursor_x = mx;
	cursor_y = my;
	printc(c);
	cursor_x = cx;
	cursor_y = cy;
}

void printk(char *string)
{
	int i;
	for (i = 0; string[i]; i++)
		printc(string[i]);
}

void clear_screen()
{
	int i, j;
	for (i = 0; i < NUM_ROWS; i++) {
		for (j = 0; j < NUM_COLUMNS; j++) {
			*(Word *)(VGA_TEXT_SCREEN_MEM + (i * NUM_COLUMNS + j) * 2) = 0;
		}
	}
}
