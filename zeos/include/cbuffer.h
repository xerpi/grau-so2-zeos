#ifndef __C_BUFFER_H__
#define __C_BUFFER_H__

#define C_SIZE 128

typedef struct {
	char carray[C_SIZE];
	unsigned int start; // points to valid data
	unsigned int end;   // points to valid data + 1
	unsigned int remain_slot;
} cbuffer_t;

/* Example
start end
|     |
A B C D E
valid data: A B C
*/
void cbuffer_init(cbuffer_t *cb);

int cbuffer_read(cbuffer_t *cb, char *buffer, unsigned int size);
int cbuffer_write(cbuffer_t *cb, char *buffer, unsigned int size);

static inline int cbuffer_put(cbuffer_t *cb, char elem)
{
	return cbuffer_write(cb, &elem, sizeof(elem));
}

static inline int cbuffer_get(cbuffer_t *cb, char *elem)
{
	return cbuffer_read(cb, elem, sizeof(elem));
}

static inline int cbuffer_nelem(cbuffer_t *cb)
{
	return C_SIZE - cb->remain_slot;
}

#endif
