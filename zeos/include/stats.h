#ifndef STATS_H
#define STATS_H

/* Structure used by 'get_stats' function */
struct stats {
	unsigned long user_ticks;
	unsigned long system_ticks;
	unsigned long blocked_ticks;
	unsigned long ready_ticks;
	unsigned long elapsed_total_ticks;
	unsigned long total_trans; /* Number of times the process has got the CPU: READY->RUN transitions */
	unsigned long remaining_ticks; /* Clock ticks */
};

struct keyboard_stats {
	unsigned int remain_read_char; // Indicates how many keys remain to read
	char *usr_buffer; // Points to first empty position of sys_read buffer
				// param when we not have enough data in
				// circular buffer. It will be used in keyboard_routine
	unsigned int total_read_char;  // number of keys requested in read syscall
};

#endif /* !STATS_H */
