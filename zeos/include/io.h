/*
 * io.h - Definició de l'entrada/sortida per pantalla en mode sistema
 */

#ifndef __IO_H__
#define __IO_H__

#include <types.h>

/** Screen functions **/
/**********************/

#define VGA_TEXT_SCREEN_MEM 0xB8000

Byte inb(unsigned short port);
void outb(unsigned short port, Byte b);
void printc(char c);
void printc_xy(Byte x, Byte y, char c);
void printk(char *string);
void clear_screen();

#endif  /* __IO_H__ */
