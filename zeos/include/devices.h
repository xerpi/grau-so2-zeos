#ifndef DEVICES_H__
#define  DEVICES_H__

#include <cbuffer.h>
#include <sched.h>

extern cbuffer_t cb;
extern int read_pending;

#define READ_BUFF_SIZE 3

void init_devices();

int sys_write_console(char *buffer, int size);
int sys_read_keyboard();


#endif /* DEVICES_H__*/
