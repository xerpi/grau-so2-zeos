/*
 * sched.h - Estructures i macros pel tractament de processos
 */

#ifndef __SCHED_H__
#define __SCHED_H__

#include <list.h>
#include <types.h>
#include <mm_address.h>
#include <stats.h>

#define NR_TASKS		10
#define KERNEL_STACK_SIZE	1024

enum state_t { ST_RUN, ST_READY, ST_BLOCKED };

struct task_struct {
	int PID;			/* Process ID. This MUST be the first field of the struct. */
	unsigned long kernel_esp;
	unsigned int heap_index;      // This points to heap_sizes table
	page_table_entry *dir_pages_baseAddr;
	int quantum;
	enum state_t state;
	struct stats stats;
	struct keyboard_stats kbs;
	struct list_head list;
};

union task_union {
	struct task_struct task;
	unsigned long stack[KERNEL_STACK_SIZE];    /* pila de sistema, per procés */
};

extern union task_union protected_tasks[NR_TASKS+2];
extern union task_union *task; /* Vector de tasques */
extern struct task_struct *idle_task;

struct heap_info_t {
	int size;
	int numrefs;
};

extern struct heap_info_t heap_infos[NR_TASKS];

#define TASK_HEAP_SIZE(t)    (heap_infos[(t)->heap_index].size)
#define TASK_HEAP_NUMREFS(t) (heap_infos[(t)->heap_index].numrefs)

extern struct list_head freequeue;
extern struct list_head readyqueue;
extern struct list_head keyboardqueue;

extern int quantum_rr;


#define KERNEL_ESP(t)	(DWord) &(t)->stack[KERNEL_STACK_SIZE]

#define INITIAL_ESP	KERNEL_ESP(&task[0])

struct task_struct *list_head_to_task_struct(struct list_head *l);

void init_stats(struct stats *s);
void init_keyboard_stats(struct keyboard_stats *kbs);

int get_free_PID();

/* Inicialitza les dades del proces inicial */
void init_task1(void);

void init_idle(void);

struct task_struct *list_pop_front(struct list_head *list);

void init_freequeue(void);
void init_readyqueue(void);
void init_keyboardqueue(void);

void init_sched(void);

struct task_struct *current();

void task_switch(union task_union* t);
void inner_task_switch(union task_union* t);

int get_position_in_task_array(struct task_struct *t);
int get_position_in_dir_pages(page_table_entry *dir);
int allocate_DIR(struct task_struct *t);
void free_DIR(struct task_struct *t);

page_table_entry * get_PT(struct task_struct *t);

page_table_entry * get_DIR(struct task_struct *t);

int get_quantum (struct task_struct *t);
void set_quantum (struct task_struct *t, int new_quantum);

void schedule();

/* Headers for the scheduling policy */

#define RR_DEFAULT_QUANTUM 10

void sched_next_rr();
void update_process_state_rr(struct task_struct *t, struct list_head *dst_queue);
int needs_sched_rr();
void update_sched_data_rr();

#endif  /* __SCHED_H__ */
