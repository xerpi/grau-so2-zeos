#ifndef __SEM_H__
#define __SEM_H__

#include "list.h"

#define SEMINFO_SIZE 20

struct seminfo_t {
	int owner;
	int value;
	int numwait;		// Number of processes remaining to finish the sem_wait
	struct list_head list;
};

extern struct seminfo_t seminfo[SEMINFO_SIZE];

void init_sem();


#endif
