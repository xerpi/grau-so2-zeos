/*
 * sys.c - Syscalls implementation
 */
#include <devices.h>
#include <utils.h>
#include <io.h>
#include <interrupt.h>
#include <mm.h>
#include <mm_address.h>
#include <sched.h>
#include <sem.h>
#include <errno.h>
#include <p_stats.h>

#define LECTURA 0
#define ESCRIPTURA 1

#define TMPBUF_SIZE 4*1024

static Byte tmpbuf[TMPBUF_SIZE];

int check_fd(int fd, int permissions)
{
	if (fd != 1)
		return -EBADF;

	if (permissions != ESCRIPTURA)
		return -EACCES;

	return 0;
}

/* BOTH USER/SYSTEM

           user_ticks_old         user_ticks_new
                  |                      |
                  v        USER          v
                  |----------------------|     user_ticks_new = user_ticks_old + elapsed

------------------>+-+-+-+-+-+-+-+-+-+-+->
elapsed_total_ticks_old / elapsed_total_ticks_new = elapsed_total_ticks_old + elapsed

                          elapsed
                  <---------------------->     elapsed = current_ticks - elapsed_total_ticks_old

----------------------------------------->
current_ticks
*/

void user_to_system(void)
{
	update_stats(&(current()->stats.user_ticks), &(current()->stats.elapsed_total_ticks));
}

void system_to_user(void)
{
	update_stats(&(current()->stats.system_ticks), &(current()->stats.elapsed_total_ticks));
}

int sys_ni_syscall()
{
	return -ENOSYS;
}

int sys_write(int fd, char *buffer, int size)
{
	int ret;
	int n;
	int nwr;

	if ((ret = check_fd(fd, ESCRIPTURA)))
		return ret;

	// Devuelve -EFAULT si el buffer es NULL (0)
	if (!access_ok(VERIFY_WRITE, buffer, size))
		return -EFAULT;

	nwr = 0;

	while (size > 0) {
		n = min(size, TMPBUF_SIZE);
		copy_from_user(buffer, tmpbuf, n);
		sys_write_console((char *)tmpbuf, n);
		size -= n;
		nwr += n;
	}

	return nwr;
}

int sys_read(int fd, char *buf, int count)
{
	int ret;
	/* We have to add support for read in check_fd
	if ((ret = check_fd(fd, LECTURA)))
		return ret;*/

	// Devuelve -EFAULT si el buffer es NULL (0)
	if (!access_ok(VERIFY_READ, buf, count))
		return -EFAULT;

	struct task_struct *cur = current();

	cur->kbs.usr_buffer = buf;
	cur->kbs.remain_read_char = count;
	cur->kbs.total_read_char = count;

	ret = sys_read_keyboard();

	return ret;
}

void *sys_sbrk(int increment)
{
	int error;
	unsigned int ret;
	int old_size;
	struct task_struct *cur = current();
	page_table_entry *cPT = get_PT(cur);
	int cur_heap_size = TASK_HEAP_SIZE(cur);


	if ((cur_heap_size + increment) < 0)
		return (void *)-ENOMEM;
	else if ((HEAP_START + (cur_heap_size + increment)) >= HEAP_END)
		return (void *)-ENOMEM;

	int cur_heap_pages = NUM_PAGES(cur_heap_size);
	int new_heap_pages = NUM_PAGES(cur_heap_size + increment);

	if (new_heap_pages > cur_heap_pages) {
		if ((alloc_and_set_chunk(cPT,
			NUM_PAGES(HEAP_START) + cur_heap_pages,
			new_heap_pages - cur_heap_pages, &ret) < 0)) {

			error = -EAGAIN;
			goto overwrite_data_pages_fail;
		}
	} else if (new_heap_pages < cur_heap_pages) {
		dealloc_and_unset_chunk(cPT,
			NUM_PAGES(HEAP_START) + new_heap_pages,
			cur_heap_pages - new_heap_pages);

		// Flush TLB
		set_cr3(get_DIR(cur));
	}

	old_size = TASK_HEAP_SIZE(cur);
	TASK_HEAP_SIZE(cur) += increment;

	return (void *)(HEAP_START + old_size);

overwrite_data_pages_fail:
	dealloc_and_unset_chunk(cPT, NUM_PAGES(HEAP_START) + cur_heap_pages, ret);
	return (void *)error;
}

int sys_gettime()
{
	return zeos_ticks;
}

int sys_getpid()
{
	return current()->PID;
}

static int ret_from_fork()
{
	return 0;
}

int sys_fork()
{
	struct task_struct *ts;
	struct task_struct *cur = current();

	unsigned int data_ret, heap_ret;
	int pag, error;

	// creates the child process
	ts = list_pop_front(&freequeue);
	if (ts == NULL)
		return -ENOMEM;

	// task_struct iguales
	//printk("COPY TASK_UNION\n");
	copy_data(cur, ts, sizeof(union task_union));

	//printk("ALLOCATE_DIR\n");
	// ts tiene su propio pagdir
	allocate_DIR(ts);

	// Copiamos todas las paginas de un proceso al otro y borramos
	// las entradas de data
	page_table_entry *cPT = get_PT(cur);
	page_table_entry *tsPT = get_PT(ts);

	//printk("COPY_PAGE_TABLE\n");
	copy_data(cPT, tsPT, TOTAL_PAGES*sizeof(page_table_entry));

	//printk("OVERWRITE TS DATA PAGES\n");
	if ((alloc_and_set_chunk(tsPT, PAG_LOG_INIT_DATA, NUM_PAG_DATA, &data_ret) < 0)) {
		error = -EAGAIN;
		goto overwrite_data_pages_fail;
	}

	// Hacemos accesibles las paginas de ts en current
	//printk("MAKE ACCESIBLE DATA PAGES TO CURRENT\n");
	copy_data(&tsPT[PAG_LOG_INIT_DATA], &cPT[PAG_LOG_INIT_DATA + NUM_PAG_DATA], NUM_PAG_DATA*sizeof(page_table_entry));

	// Copiamos los datos
	//printk("COPY DATA TO TS\n");
	copy_data((void *)DATA_START, (void *)(DATA_END), DATA_END - DATA_START);

	int cur_heap_size = TASK_HEAP_SIZE(cur);
	int cur_heap_pages = NUM_PAGES(cur_heap_size);

	ts->heap_index = get_position_in_task_array(ts);
	// Set the same heap size as the parent (child will copy parent's heap)
	TASK_HEAP_SIZE(ts) = cur_heap_size;
	// Set the number of references to the heap to 1
	TASK_HEAP_NUMREFS(ts) = 1;

	// Allocate heap pages for the child
	if ((alloc_and_set_chunk(tsPT, NUM_PAGES(HEAP_START), cur_heap_pages, &heap_ret) < 0)) {
		error = -EAGAIN;
		goto alloc_heap_pages_fail;
	}

	// Map and copy heap
	char done = 0; // Indicates if we have maped and copied all heap
	int page_idx = 0;
	int nmin;
	while (!done) {
		nmin = min(NUM_PAG_DATA, cur_heap_pages - page_idx);
		copy_data(&tsPT[NUM_PAGES(HEAP_START) + page_idx],
			&cPT[PAG_LOG_INIT_DATA + NUM_PAG_DATA],
			nmin*sizeof(page_table_entry));
		set_cr3(get_DIR(cur));
		copy_data((void *)(HEAP_START + (page_idx*PAGE_SIZE)), (void *)(DATA_END), nmin*PAGE_SIZE);

		page_idx += nmin;

		done = (cur_heap_pages == page_idx);
	}

	// Borramos las entradas de ts en current
	//printk("ERASE TS DATA PAGES IN CURRENT\n");
	for (pag = 0; pag < NUM_PAG_DATA; pag++){
		del_ss_pag(cPT, PAG_LOG_INIT_DATA + NUM_PAG_DATA + pag);
	}

	// Flush TLB
	set_cr3(get_DIR(cur));

	long ebp_current;
	__asm__ __volatile__(
		"mov %%ebp,%0;\n\t"
		: "=m" (ebp_current)
		:
		:
	);

	// Task_struct layout in 'sys_fork':
	//  PCB: kernel_esp ---------   When we task_switch:
	//                          |   pop ebp
	//  ebp                   <-|   ret (ret_from_fork moves to eax 0 for child)
	//  &ret_from_fork
	//  @ret_handler

	//  %ebx   <-|
	//  %ecx     |
	//  %edx     |
	//  %esi     | Register saved
	//  %edi     |  by 'save'
	//  %ebp     |
	//  %eax     |
	//  %ds      |
	//  %es      |
	//  %fs      |
	//  %gs    <-|

	//  %eip   <-|
	//  %cs      |
	//  %eflags  |  Return context saved
	//  %oldesp  |   by the processor.
	//  %oldss <-|
	//

	// Calculamos la posicion (array) y la diferencia (address)
	unsigned int pos = ((unsigned int)ebp_current - (unsigned int)cur)/4;
	unsigned int diff = pos*sizeof(DWord);
	ts->kernel_esp = (unsigned int)ts + diff - sizeof(DWord); // Le resto 4 para poner &ret_from_fork

	union task_union *tu = (union task_union *)ts;
	//tu->stack[pos + 8] = 0; // Ponemos el eax del SAVE_ALL a 0 || NO HACE FALTA, ya que despues llega al handler que pone eax en el SAVED(eax)...

	tu->stack[pos] = (unsigned int)&ret_from_fork; // Hay que poner esto para que mueva un 0 a eax, que luego se pondra en el handler en SAVED(eax)
	tu->stack[pos - 1] = 0; // Puede ser cualquier valor :D

	// Set child's new PID
	ts->PID = get_free_PID();
	ts->state = ST_READY;

	// Set statistics
	init_stats(&ts->stats);

	// Add the new process to the readyqueue
	list_add_tail(&ts->list, &readyqueue);

	//printk("fork() DONE\n");

	return ts->PID;

alloc_heap_pages_fail:
	dealloc_and_unset_chunk(tsPT, NUM_PAGES(HEAP_START), heap_ret);

overwrite_data_pages_fail:
	dealloc_and_unset_chunk(tsPT, PAG_LOG_INIT_DATA, data_ret);

	// We have to reinsert task_struct in freequeue
	list_add_tail(&ts->list, &freequeue);
	return error;
}

int sys_clone(void (*function)(void), void *stack)
{
	unsigned int parent_dir_pos;
	page_table_entry *pd_parent;
	struct task_struct *ts;
	struct task_struct *cur = current();

	// creates the child process
	ts = list_pop_front(&freequeue);
	if (ts == NULL)
		return -ENOMEM;

	// task_struct iguales
	copy_data(cur, ts, sizeof(union task_union));

	// the child shares its parent's dir, so increase the refcount
	pd_parent = get_DIR(cur);
	parent_dir_pos = get_position_in_dir_pages(pd_parent);
	dir_pages_refs[parent_dir_pos]++;

	// Increase the number of references to the heap
	TASK_HEAP_NUMREFS(cur)++;

	long ebp_current;
	__asm__ __volatile__(
		"mov %%ebp,%0;\n\t"
		: "=m" (ebp_current)
		:
		:
	);

	// Task_struct layout in 'sys_fork':
	//  PCB: kernel_esp ---------   When we task_switch:
	//                          |   pop ebp
	//  ebp                   <-|   ret (ret_from_fork moves to eax 0 for child)
	//  @ret_handler

	//  %ebx   <-|
	//  %ecx     |
	//  %edx     |
	//  %esi     | Register saved
	//  %edi     |  by 'save'
	//  %ebp     |
	//  %eax     |
	//  %ds      |
	//  %es      |
	//  %fs      |
	//  %gs    <-|

	//  %eip   <-|
	//  %cs      |
	//  %eflags  |  Return context saved
	//  %oldesp  |   by the processor.
	//  %oldss <-|
	//

	// Calculamos la posicion (array) y la diferencia (address)
	unsigned int pos = ((unsigned int)ebp_current - (unsigned int)cur)/4;
	unsigned int diff = pos*sizeof(DWord);
	ts->kernel_esp = (unsigned int)ts + diff; // No hace falta ret_from_fork, ya que el thread no recibe nada

	union task_union *tu = (union task_union *)ts;

	tu->stack[pos] = 0; // Puede ser cualquier valor :D

	// Change usermode context, %eip <- function and %esp <- stack_base %ebp <- stack_base
	tu->stack[KERNEL_STACK_SIZE - 2] = tu->stack[KERNEL_STACK_SIZE - 11] = (unsigned long)stack;
	tu->stack[KERNEL_STACK_SIZE - 5] = (unsigned long)function;

	// Set child's new PID
	ts->PID = get_free_PID();
	ts->state = ST_READY;

	// Set statistics
	init_stats(&ts->stats);

	// Add the new process to the readyqueue
	list_add_tail(&ts->list, &readyqueue);

	return ts->PID;
}

void sys_exit()
{
	struct task_struct *cur = current();

	free_DIR(cur);

	// Decrement the number of references to the heap
	TASK_HEAP_NUMREFS(cur)--;

	// If we have 0 references to the heap, we can delete it
	if (TASK_HEAP_NUMREFS(cur) == 0) {
		dealloc_and_unset_chunk(get_PT(cur), PH_PAGE(HEAP_START),
			NUM_PAGES(TASK_HEAP_SIZE(cur)));
	}

	// Add the process PCB to the freequeue
	list_add_tail(&cur->list, &freequeue);

	sched_next_rr();
}

int sys_get_stats(int pid, struct stats *st)
{
	int i;

	for (i = 0; i < NR_TASKS; i++) {
		if (task[i].task.PID == pid) {
			task[i].task.stats.remaining_ticks = quantum_rr;
			copy_to_user(&task[i].task.stats, st, sizeof(struct stats));
			return 0;
		}
	}

	return -ESRCH;
}

int sys_sem_init(int n_sem, unsigned int value)
{
	if (n_sem < 0 || n_sem >= SEMINFO_SIZE)
		return -EBADSLT;

	if (seminfo[n_sem].owner != -1)
		return -EBUSY;

	if (seminfo[n_sem].numwait > 0) {
		do {
			update_process_state_rr(current(), &readyqueue);
			sched_next_rr();
		} while (seminfo[n_sem].numwait > 0);
	}

	seminfo[n_sem].owner = current()->PID;
	seminfo[n_sem].value = value;
	INIT_LIST_HEAD(&seminfo[n_sem].list);

	return 0;
}

int sys_sem_wait(int n_sem)
{
	if (n_sem < 0 || n_sem >= SEMINFO_SIZE)
		return -EBADSLT;

	if (seminfo[n_sem].owner == -1)
		return -EBADSLT;

	seminfo[n_sem].value--;
	seminfo[n_sem].numwait++;

	if (seminfo[n_sem].value < 0) {
		update_process_state_rr(current(), &seminfo[n_sem].list);
		sched_next_rr();
	}

	seminfo[n_sem].numwait--;

	if (seminfo[n_sem].owner == -1)
		return -EBADSLT;

	return 0;
}

int sys_sem_signal(int n_sem)
{
	struct task_struct *t;

	if (n_sem < 0 || n_sem >= SEMINFO_SIZE)
		return -EBADSLT;

	if (seminfo[n_sem].owner == -1)
		return -EBADSLT;

	seminfo[n_sem].value++;

	if (seminfo[n_sem].value <= 0) {
		t = list_pop_front(&seminfo[n_sem].list);
		list_add_tail(&(t->list), &readyqueue);
	}

	return 0;
}

int sys_sem_destroy(int n_sem)
{
	struct list_head *it;
	struct list_head *tmp;

	if (n_sem < 0 || n_sem >= SEMINFO_SIZE)
		return -EBADSLT;

	if (seminfo[n_sem].owner == -1)
		return -EBADSLT;

	list_for_each_safe(it, tmp, &seminfo[n_sem].list) {
		list_del(it);
		list_add_tail(it, &readyqueue);
	}

	seminfo[n_sem].owner = -1;

	return 0;
}
