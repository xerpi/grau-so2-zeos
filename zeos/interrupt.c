/*
 * interrupt.c -
 */
#include <types.h>
#include <interrupt.h>
#include <segment.h>
#include <hardware.h>
#include <io.h>
#include <devices.h>

#include <zeos_interrupt.h>

Gate idt[IDT_ENTRIES];
Register    idtR;
int zeos_ticks;

/* The following array is taken from
http://www.osdever.net/bkerndev/Docs/keyboard.htm
All credits where due
*/
static const unsigned char char_map[128] = {
	0, 27, '1', '2', '3', '4', '5', '6', '7', '8', /* 9 */
	'9', '0', '-', '=', '\b', /* Backspace */
	'\t', /* Tab */
	'q', 'w', 'e', 'r', /* 19 */
	't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n', /* Enter key */
	0, /* 29 - Control */
	'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', /* 39 */
	'\'', '`', 0, /* Left shift */
	'\\', 'z', 'x', 'c', 'v', 'b', 'n', /* 49 */
	'm', ',', '.', '/', 0, /* Right shift */
	'*',
	0, /* Alt */
	' ', /* Space bar */
	0, /* Caps lock */
	0, /* 59 - F1 key ... > */
	0, 0, 0, 0, 0, 0, 0, 0,
	0, /* < ... F10 */
	0, /* 69 - Num lock*/
	0, /* Scroll Lock */
	0, /* Home key */
	0, /* Up Arrow */
	0, /* Page Up */
	'-',
	0, /* Left Arrow */
	0,
	0, /* Right Arrow */
	'+',
	0, /* 79 - End key*/
	0, /* Down Arrow */
	0, /* Page Down */
	0, /* Insert Key */
	0, /* Delete Key */
	0, 0, 0,
	0, /* F11 Key */
	0, /* F12 Key */
	0, /* All other keys are undefined */
};
void setInterruptHandler(int vector, void (*handler)(), int maxAccessibleFromPL)
{
	/***********************************************************************/
	/* THE INTERRUPTION GATE FLAGS:                          R1: pg. 5-11  */
	/* ***************************                                         */
	/* flags = x xx 0x110 000 ?????                                        */
	/*         |  |  |                                                     */
	/*         |  |   \ D = Size of gate: 1 = 32 bits; 0 = 16 bits         */
	/*         |   \ DPL = Num. higher PL from which it is accessible      */
	/*          \ P = Segment Present bit                                  */
	/***********************************************************************/
	Word flags = (Word)(maxAccessibleFromPL << 13);
	flags |= 0x8E00;    /* P = 1, D = 1, Type = 1110 (Interrupt Gate) */

	idt[vector].lowOffset       = lowWord((DWord)handler);
	idt[vector].segmentSelector = __KERNEL_CS;
	idt[vector].flags           = flags;
	idt[vector].highOffset      = highWord((DWord)handler);
}

void setTrapHandler(int vector, void (*handler)(), int maxAccessibleFromPL)
{
	/***********************************************************************/
	/* THE TRAP GATE FLAGS:                                  R1: pg. 5-11  */
	/* ********************                                                */
	/* flags = x xx 0x111 000 ?????                                        */
	/*         |  |  |                                                     */
	/*         |  |   \ D = Size of gate: 1 = 32 bits; 0 = 16 bits         */
	/*         |   \ DPL = Num. higher PL from which it is accessible      */
	/*          \ P = Segment Present bit                                  */
	/***********************************************************************/
	Word flags = (Word)(maxAccessibleFromPL << 13);

	//flags |= 0x8F00;    /* P = 1, D = 1, Type = 1111 (Trap Gate) */
	/* Changed to 0x8e00 to convert it to an 'interrupt gate' and so
	the system calls will be thread-safe. */
	flags |= 0x8E00;    /* P = 1, D = 1, Type = 1110 (Interrupt Gate) */

	idt[vector].lowOffset       = lowWord((DWord)handler);
	idt[vector].segmentSelector = __KERNEL_CS;
	idt[vector].flags           = flags;
	idt[vector].highOffset      = highWord((DWord)handler);
}

extern void clock_handler();
extern void keyboard_handler();
extern void system_call_handler();

void clock_routine()
{
	zeos_show_clock();
	++zeos_ticks;
	schedule();
}

/* Register 0x60 - Keyboard input data
 * 8         7                   0
 * +---------+---------+---------+
 * |!Pressed |     Character     |
 * +---------+---------+---------+
 */

int aux = 0;

void keyboard_routine()
{
	Byte c = inb(0x60);
	char ascii = char_map[c & 0x7F];
	struct task_struct *ts;

	if (c & 0x80) {
		// Released
	} else {
		// Pressed
		// 'C' means not valid ascii char
		ascii = (ascii == '\0') ? 'C' : ascii;
		//printc(ascii);

		cbuffer_put(&cb, ascii);
		if (!list_empty(&keyboardqueue)) {
			ts = list_pop_front(&keyboardqueue);
			if (cbuffer_nelem(&cb) >= READ_BUFF_SIZE || cbuffer_nelem(&cb) >= ts->kbs.remain_read_char) {
				// For coherence we put ST_READY
				read_pending = 1;
				ts->state = ST_READY;
				update_process_state_rr(ts, &readyqueue);
			} else {
				list_add(&(ts->list), &keyboardqueue);
			}
		}
	}
}

void setIdt()
{
	/* Program interrups/exception service routines */
	idtR.base  = (DWord)idt;
	idtR.limit = IDT_ENTRIES * sizeof(Gate) - 1;

	set_handlers();

	/* ADD INITIALIZATION CODE FOR INTERRUPT VECTOR */
	setInterruptHandler(32, clock_handler, 0);
	setInterruptHandler(33, keyboard_handler, 0);

	setTrapHandler(0x80, system_call_handler, 3);

	set_idt_reg(&idtR);
}
