/*
 * sched.c - initializes struct for task 0 anda task 1
 */

#include <sched.h>
#include <mm.h>
#include <io.h>
#include <interrupt.h>
#include <p_stats.h>
#include <utils.h>

/**
 * Container for the Task array and 2 additional pages (the first and the last one)
 * to protect against out of bound accesses.
 */
union task_union protected_tasks[NR_TASKS+2]
	__attribute__((__section__(".data.task")));

union task_union *task = &protected_tasks[1]; /* == union task_union task[NR_TASKS] */

struct task_struct *idle_task;

struct heap_info_t heap_infos[NR_TASKS];

struct list_head freequeue;
struct list_head readyqueue;
struct list_head keyboardqueue;

static int last_PID;

int quantum_rr;

extern struct list_head blocked;

struct task_struct *list_head_to_task_struct(struct list_head *l)
{
	return list_entry(l, struct task_struct, list);
}

void init_stats(struct stats *s)
{
	s->user_ticks = 0;
	s->system_ticks = 0;
	s->blocked_ticks = 0;
	s->ready_ticks = 0;
	s->elapsed_total_ticks = get_ticks();
	s->total_trans = 0;

	// It would have correct value user
	// call sys_get_stats to get statistics
	s->remaining_ticks = get_ticks();
}

void init_keyboard_stats(struct keyboard_stats *kbs)
{
	kbs->remain_read_char = 0;
	kbs->usr_buffer = NULL;
	kbs->total_read_char = 0;
}

int get_free_PID()
{
	return ++last_PID;
}

/* get_DIR - Returns the Page Directory address for task 't' */
page_table_entry *get_DIR(struct task_struct *t)
{
	return t->dir_pages_baseAddr;
}

/* get_PT - Returns the Page Table address for task 't' */
page_table_entry *get_PT(struct task_struct *t)
{
	return (page_table_entry *)(((unsigned int)(t->dir_pages_baseAddr->bits.pbase_addr))<<12);
}

int get_position_in_task_array(struct task_struct *t)
{
	return ((int)t-(int)task)/sizeof(union task_union);
}

int get_position_in_dir_pages(page_table_entry *dir)
{
	return ((int)dir-(int)dir_pages)/(sizeof(page_table_entry) * TOTAL_PAGES);
}

int allocate_DIR(struct task_struct *t)
{
	int pos;

	pos = get_position_in_task_array(t);

	t->dir_pages_baseAddr = (page_table_entry *)&dir_pages[pos];

	dir_pages_refs[pos] = 1;

	return 1;
}

void free_DIR(struct task_struct *t)
{
	int dir_pos;
	page_table_entry *pd;
	page_table_entry *pt;

	pd = get_DIR(t);
	dir_pos = get_position_in_dir_pages(pd);

	/* Decrease refcount */
	dir_pages_refs[dir_pos]--;

	if (dir_pages_refs[dir_pos] == 0) {
		pt = get_PT(t);
		dealloc_and_unset_chunk(pt, PAG_LOG_INIT_DATA, NUM_PAG_DATA);
	}
}

void cpu_idle(void)
{
	// Commented for test if task_switch works...
	__asm__ __volatile__("sti": : :"memory");

	while(1) {
		//printk("Idle process\n"); // for testing
	}
}

/*
 ____________
|            |
|     PCB    |
|____________|
|            |
|            |
|            |
|            |
|____________|
|      0     |
|____________|
|  @cpu_idle |
|____________|

*/

void init_idle(void)
{
	idle_task = list_pop_front(&freequeue);

	idle_task->PID = 0;
	set_quantum(idle_task, 0); // ¿? 0 to identify idle_task?
	idle_task->state = ST_RUN; // ¿?

	idle_task->heap_index = -1;

	init_stats(&idle_task->stats);
	init_keyboard_stats(&idle_task->kbs);

	allocate_DIR(idle_task);

	union task_union *tu = (union task_union *)idle_task;

	tu->stack[KERNEL_STACK_SIZE - 1] = (unsigned long)&cpu_idle;
	tu->stack[KERNEL_STACK_SIZE - 2] = 0;
	tu->task.kernel_esp = (unsigned long)&tu->stack[KERNEL_STACK_SIZE - 2];

}

void init_task1(void)
{
	struct task_struct *ts = list_pop_front(&freequeue);

	ts->PID = 1;
	set_quantum(ts, RR_DEFAULT_QUANTUM);
	ts->state = ST_RUN;

	ts->heap_index = get_position_in_task_array(ts);

	// Set the number of references to the heap to 1
	TASK_HEAP_NUMREFS(ts) = 1;

	init_stats(&ts->stats);
	init_keyboard_stats(&ts->kbs);

	quantum_rr = get_quantum(ts);

	allocate_DIR(ts);
	set_cr3(get_DIR(ts));
	set_user_pages(ts);

	union task_union *tu = (union task_union *)ts;

	tss.esp0 = (unsigned long)&tu->stack[KERNEL_STACK_SIZE];
}

struct task_struct *list_pop_front(struct list_head *list)
{
	struct list_head *l;

	if (list_empty(list))
		return NULL;

	l = list_first(list);
	list_del(l);

	return list_head_to_task_struct(l);
}

void init_freequeue(void)
{
	INIT_LIST_HEAD(&freequeue);

	int i;
	for (i = 0; i < NR_TASKS; ++i)
		list_add_tail(&task[i].task.list, &freequeue);
}

void init_readyqueue(void)
{
	INIT_LIST_HEAD(&readyqueue);
}

void init_keyboardqueue(void)
{
	INIT_LIST_HEAD(&keyboardqueue);
}

void init_sched()
{
	int i;

	for (i = 0; i < NR_TASKS; i++) {
		task[i].task.PID = -1;
		heap_infos[i].size = 0;
		heap_infos[i].numrefs = 0;
	}

	init_freequeue();
	init_readyqueue();
	init_keyboardqueue();

	last_PID = 1; // Skip idle and task1 process
}

struct task_struct* current()
{
	int ret_value;

	__asm__ __volatile__(
		"movl %%esp, %0"
		: "=g" (ret_value));

	return (struct task_struct*)(ret_value&0xfffff000);
}

void task_switch(union task_union* t)
{
	__asm__ __volatile__ (
		"pushl %%esi\n\t"
		"pushl %%edi\n\t"
		"pushl %%ebx\n\t"
		:
		:
	);
	inner_task_switch(t);
	__asm__ __volatile__ (
		"popl %%ebx\n\t"
		"popl %%edi\n\t"
		"popl %%esi\n\t"
		:
		:
	);
}

void inner_task_switch(union task_union* t)
{
	tss.esp0 = (unsigned long)&t->stack[KERNEL_STACK_SIZE];
	set_cr3(get_DIR((struct task_struct *)&t->task));

	struct task_struct* c = current();
	__asm__ __volatile__ (
		//"pushl %%ebp\n\t"       // pushl ebp        NO NECESARIO PORQUE EL COMPILADOR
		//"movl %%esp, %%ebp\n\t" // ebp <- esp       LO HACE AUTOMATICAMENTE
		"movl %%ebp, %0\n\t"    // kernel_esp <- ebp
		"movl %1, %%esp\n\t"    // esp <- new.kernel_esp
		"popl %%ebp\n\t"
		"ret\n\t"
		:
		: "m" (c->kernel_esp), "m" (t->task.kernel_esp)
	);

}

int get_quantum(struct task_struct *t)
{
	return t->quantum;
}

void set_quantum(struct task_struct *t, int new_quantum)
{
	t->quantum = new_quantum;
}

void schedule()
{
	update_sched_data_rr();

	if (needs_sched_rr()) {
		update_process_state_rr(current(), &readyqueue);
		sched_next_rr();
	}
}

/* RR scheduling policy */

void sched_next_rr()
{
	struct task_struct *new;

	new = list_pop_front(&readyqueue);
	if (!new) { // temporary workaround
		new = idle_task;
	}

	new->state = ST_RUN;

	quantum_rr = get_quantum(new);
	update_stats(&(current()->stats.system_ticks), &(current()->stats.elapsed_total_ticks));
	update_stats(&(new->stats.ready_ticks), &(new->stats.elapsed_total_ticks));
	new->stats.total_trans++;

	task_switch((union task_union *)new);
}

void update_process_state_rr(struct task_struct *t, struct list_head *dst_queue)
{
	// Idle process does not to be in any queue
	if (t && t->PID != 0) {
		if (dst_queue != NULL) {
			list_add_tail(&(t->list), dst_queue);
			if (dst_queue != &readyqueue) t->state = ST_BLOCKED;
			else {
				update_stats(&(current()->stats.system_ticks), &(current()->stats.elapsed_total_ticks));
				t->state = ST_READY;
			}
		}
		else t->state = ST_RUN;
	}
}

int needs_sched_rr()
{
	struct task_struct *cur;

	cur = current();

	/* If we are running the idle task and there's some
	 * task waiting in the readyqueue, switch to it */
	if (cur->PID == 0 && !list_empty(&readyqueue)) {
		return 1;
	}

	return (quantum_rr == 0);
}

void update_sched_data_rr()
{
	--quantum_rr;
}
