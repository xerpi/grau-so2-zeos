/*
 * libc.c
 */

#include <libc.h>

#include <types.h>

int errno;

void reverse(char *s)
{
	int i, j;
	char c;

	for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

void itoa(int n, char *s)
{
	int i, sign;

	if ((sign = n) < 0)
		n = -n;

	i = 0;
	do {
		s[i++] = n % 10 + '0';
	} while ((n /= 10) > 0);

	if (sign < 0)
		s[i++] = '-';

	s[i] = '\0';
	reverse(s);
}

int strlen(const char *a)
{
	int i;

	i = 0;

	while (a[i] != 0)
		i++;

	return i;
}

int printf(const char *s)
{
	return write(1, s, strlen(s));
}

/* Syscall wrappers */

// Parameter order: ebx, ecx, edx, esi, edi

int write(int fd, const char *buffer, int size)
{
	long ret;
	__asm__ __volatile__(
		"int $0x80\n\t"
		: "=a"(ret)
		: "a"(4), "b"(fd), "c"(buffer), "d"(size)
		:
	);

	if (ret < 0) {
		errno = ret;
		ret = -1;
	}

	return ret;
}

int read(int fd, char *buf, int count)
{
	long ret;
	__asm__ __volatile__(
		"int $0x80\n\t"
		: "=a"(ret)
		: "a"(5), "b"(fd), "c"(buf), "d"(count)
		:
	);

	if (ret < 0) {
		errno = ret;
		ret = -1;
	}

	return ret;
}

void *sbrk(int increment)
{
	long ret;
	__asm__ __volatile__(
		"int $0x80\n\t"
		: "=a"(ret)
		: "a"(6), "b"(increment)
		:
	);

	if (ret < 0) {
		errno = ret;
		ret = -1;
	}

	return (void *)ret;
}


int gettime()
{
	long ret;
	__asm__ __volatile__(
		"int $0x80\n\t"
		: "=a"(ret)
		: "a"(10)      // eax = 10
		:
	);

	return ret;
}

int getpid()
{
	long ret;
	__asm__ __volatile__(
		"int $0x80\n\t"
		: "=a"(ret)
		: "a"(20)      // eax = 20
		:
	);

	return ret;
}

int fork()
{
	long ret;
	__asm__ __volatile__(
		"int $0x80\n\t"
		: "=a"(ret)
		: "a"(2)      // eax = 2
		:
	);

	if (ret < 0) {
		errno = ret;
		ret = -1;
	}

	return ret;
}

int clone(void (*function)(void), void *stack)
{
	long ret;
	__asm__ __volatile__(
		"int $0x80\n\t"
		: "=a"(ret)
		: "a"(19), "b"(function), "c"(stack)
		:
	);

	if (ret < 0) {
		errno = ret;
		ret = -1;
	}

	return ret;
}

void exit()
{
	__asm__ __volatile__(
		"int $0x80\n\t"
		:
		: "a"(1)      // eax = 1
		:
	);
}

int get_stats(int pid, struct stats *st)
{
	long ret;
	__asm__ __volatile__(
		"int $0x80\n\t"
		: "=a"(ret)
		: "a"(35), "b"(pid), "c"(st)
		:
	);

	if (ret < 0) {
		errno = ret;
		ret = -1;
	}

	return ret;
}

int sem_init(int n_sem, unsigned int value)
{
	long ret;
	__asm__ __volatile__(
		"int $0x80\n\t"
		: "=a"(ret)
		: "a"(21), "b"(n_sem), "c"(value)
		:
	);

	if (ret < 0) {
		errno = ret;
		ret = -1;
	}

	return ret;
}

int sem_wait(int n_sem)
{
	long ret;
	__asm__ __volatile__(
		"int $0x80\n\t"
		: "=a"(ret)
		: "a"(22), "b"(n_sem)
		:
	);

	if (ret < 0) {
		errno = ret;
		ret = -1;
	}

	return ret;
}

int sem_signal(int n_sem)
{
	long ret;
	__asm__ __volatile__(
		"int $0x80\n\t"
		: "=a"(ret)
		: "a"(23), "b"(n_sem)
		:
	);

	if (ret < 0) {
		errno = ret;
		ret = -1;
	}

	return ret;
}

int sem_destroy(int n_sem)
{
	long ret;
	__asm__ __volatile__(
		"int $0x80\n\t"
		: "=a"(ret)
		: "a"(24), "b"(n_sem)
		:
	);

	if (ret < 0) {
		errno = ret;
		ret = -1;
	}

	return ret;
}
